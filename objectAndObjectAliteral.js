var student = {
    firstName: "Nguyen",
    lastName: "Teo",
    greet: function() {
        console.log('Hi ' + this.firstName + " " + this.lastName);
    }
}
student.greet();

console.log(student['firstName']);