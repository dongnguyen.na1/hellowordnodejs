//when we have more require we should be wrap all it a folder. So code is clean a easy maintan
var spanish = require('./spanish');
var eng = require('./english');

// export all
module.exports = {
    spanish: spanish,
    eng: eng,
};