//function statement
function greet() {
    console.log('hi Dong');
}
greet(); // call function greet
//function first-class
function loggreet(fn) {
    fn();
}
loggreet(greet); // call a function in a function

// function expression

var myGreet = function() {
    console.log('Hi Nodejs');
};
myGreet();

// first-class one more time
loggreet(myGreet);

var forgeinGreet = require('./formodule');
loggreet(forgeinGreet);

//function Contructor
function functionContructor(firstname, lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
}
functionContructor.prototype.greeted = function() {
    console.log('Hi ' + this.firstname + ' ' + this.lastname);
};
var Dong = new functionContructor('dong', 'nguyen');
Dong.greeted();
var Lia = new functionContructor('Lia', 'Nguyen');
Lia.greeted();