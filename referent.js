//pass by value
function change(b) {
    b = 2;
}
var a = 1;
change(a);
console.log(a);

//pass by reference
function changeOj(d) {
    d.prop1 = function() {};
    d.prop2 = {};
}
var c = {};
c.prop1 = {};
changeOj(c);
console.log(c);